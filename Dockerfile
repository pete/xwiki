FROM tomcat:8-jdk11-adoptopenjdk-hotspot

#    ____  ____  ____      ____  _   __        _
#   |_  _||_  _||_  _|    |_  _|(_) [  |  _   (_)
#     \\ \\  / /    \\ \\  /\\  / /  __   | | / ]  __
#      > `' <      \\ \\/  \\/ /  [  |  | '' <  [  |
#    _/ /'`\\ \\_     \\  /\\  /    | |  | |`\\ \\  | |
#   |____||____|     \\/  \\/    [___][__|  \\_][___]

MAINTAINER Pablo Gonzalez <pablo.gonzalez.jimenez@cern.ch>

# Install LibreOffice + other tools
# Note that procps is required to get ps which is used by JODConverter to start LibreOffice
RUN apt-get update && apt-get --no-install-recommends -y install \
    curl \
    libreoffice \
    unzip \
    procps \
    libmysql-java \
    vim \
    && rm -rf /var/lib/apt/lists/*


# Install XWiki as the ROOT webapp context in Tomcat
# Create the Tomcat temporary directory
# Configure the XWiki permanent directory
ARG XWIKI_VERSION=12.3
ENV XWIKI_VERSION		${XWIKI_VERSION}
ENV XWIKI_URL_PREFIX 		https://maven.xwiki.org/releases/org/xwiki/platform/xwiki-platform-distribution-war/${XWIKI_VERSION}
ARG XWIKI_DOWNLOAD_SHA256=300d1daef9daa0bab238a4c7802cb65dd904b1ec93dea4cd240e9c835f66aa1c
ENV XWIKI_DOWNLOAD_SHA256	${XWIKI_DOWNLOAD_SHA256}

ARG DB_HOST=dbod-twiki.cern.ch
ENV DB_HOST		${DB_HOST}
ARG DB_DATABASE=xwiki
ENV DB_DATABASE		${DB_DATABASE}
ARG DB_USER=xwiki
ENV DB_USER		${DB_USER} 
ARG DB_PASSWORD=xwiki
ENV DB_PASSWORD 	${DB_PASSWORD}

ARG INDEX_HOST=localhost
ENV INDEX_HOST          ${INDEX_HOST}
ARG INDEX_PORT=8983
ENV INDEX_PORT          ${INDEX_PORT}

RUN rm -rf /usr/local/tomcat/webapps/* && \
 	rm -rf /usr/local/xwiki && \
 	mkdir -p /usr/local/tomcat/temp && \
	ln -s /xwiki /usr/local/xwiki && \
 	curl -fSL "${XWIKI_URL_PREFIX}/xwiki-platform-distribution-war-${XWIKI_VERSION}.war" -o xwiki.war && \
 	echo "${XWIKI_DOWNLOAD_SHA256} xwiki.war" | sha256sum -c - && \
	unzip -d /usr/local/tomcat/webapps/ROOT xwiki.war && \
	rm -f xwiki.war

RUN set -x && \
	cp /usr/share/java/mysql-connector-java-*.jar /usr/local/tomcat/webapps/ROOT/WEB-INF/lib/

# Configure Tomcat. For example set the memory for the Tomcat JVM since the default value is too small for XWiki
COPY tomcat/setenv.sh /usr/local/tomcat/bin/
RUN chmod -R 777 /usr/local/tomcat

# Setup the XWiki Hibernate configuration
COPY xwiki/hibernate.cfg.xml /usr/local/tomcat/webapps/ROOT/WEB-INF/hibernate.cfg.xml

# Set a specific distribution id in XWiki for this docker packaging.
RUN sed -i 's/<id>org.xwiki.platform:xwiki-platform-distribution-war/<id>org.xwiki.platform:xwiki-platform-distribution-docker/' \
	/usr/local/tomcat/webapps/ROOT/META-INF/extension.xed

# Add scripts required to make changes to XWiki configuration files at execution time
# Note: we don't run CHMOD since 1) it's not required since the executabe bit is already set in git and 2) running
# CHMOD after a COPY will sometimes fail, depending on different host-specific factors (especially on AUFS).
COPY xwiki/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# OPENID
COPY xwiki/openid.cfg /tmp/openid.cfg
COPY jars/oidc/* /tmp/oicd_jars/

# Make the XWiki directory (the permanent directory is included in it) persist on the host (so that it's not recreated
# across runs)
#VOLUME /usr/local/xwiki



# Starts XWiki by starting Tomcat. All options passed to docker-entrypoint.sh. 
# If "xwiki" is passed then XWiki will be configured the first time the
# container executes and Tomcat will be started. If some other parameter is passed then it'll be executed to comply
# with best practices defined at https://github.com/docker-library/official-images#consistency.

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["xwiki"]
